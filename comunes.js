var comunes = (() => {
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __export = (target, all) => {
    for (var name in all)
      __defProp(target, name, { get: all[name], enumerable: true });
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

  // comunes.mjs
  var comunes_exports = {};
  __export(comunes_exports, {
    LICENCIA: () => LICENCIA,
    Libro: () => Libro,
    er_grupolibro: () => er_grupolibro,
    er_unlibropos: () => er_unlibropos,
    textoIncluyeRefBiblica: () => textoIncluyeRefBiblica
  });
  var LICENCIA = `
ISC License

Copyright (c) 2023, Vladimir T\xE1mara Pati\xF1o vtamara@pasosdeJesus.org

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR 
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF 
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
`;
  var Libro = {
    "G\xE9.": "G\xE9nesis",
    "\xC9x.": "\xC9xodo",
    "Lev.": "Lev\xEDtico",
    "N\xFA.": "N\xFAmeros",
    "Dt.": "Deuteronomio",
    "Jos.": "Josu\xE9",
    "Jue.": "Jueces",
    "Rut": "Rut",
    "1 Sa.": "1 Samuel",
    "2 Sa.": "2 Samuel",
    "1 Re.": "1 Reyes",
    "2 Re.": "2 Reyes",
    "1 Cr.": "1 Cr\xF3nicas",
    "2 Cr.": "2 Cr\xF3nicas",
    "Esd.": "Esdras",
    "Ne.": "Nehem\xEDas",
    "Est.": "Esther",
    "Job": "Job",
    "Sal.": "Salmos",
    "Pr.": "Proverbios",
    "Ec.": "Eclesiast\xE9s",
    "C.C.": "Cantar de los Cantares",
    "Isa.": "Isa\xEDas",
    "Jer.": "Jerem\xEDas",
    "La.": "Lamentaciones",
    "Eze.": "Ezekiel",
    "Da.": "Daniel",
    "Os.": "Oseas",
    "Joel": "Joel",
    "Am.": "Am\xF3s",
    "Ab.": "Abd\xEDas",
    "Jon.": "Jon\xE1s",
    "Miq.": "Miqueas",
    "Na.": "Nahum",
    "Hab.": "Habacuc",
    "Sof.": "Sofon\xEDas",
    "Hag.": "Hageo",
    "Zac.": "Zacar\xEDas",
    "Mal.": "Malaqu\xEDas",
    "Mt.": "Mateo",
    "Mc.": "Marcos",
    "Lc.": "Lucas",
    "Jn.": "Juan",
    "Hec.": "Hechos",
    "Ro.": "Romanos",
    "1 Co.": "1 Corintios",
    "2 Co.": "2 Corintios",
    "G\xE1l.": "G\xE1latas",
    "Efe.": "Efesios",
    "Flp.": "Filipenses",
    "Col.": "Colosenses",
    "1 Te.": "1 Tesalonicenses",
    "2 Te.": "2 Tesalonicenses",
    "1 Ti.": "1 Timoteo",
    "2 Ti.": "2 Timoteo",
    "Tit.": "Tito",
    "Flm.": "Filem\xF3n",
    "Heb.": "Hebreos",
    "Snt.": "Santiago",
    "1 Pe.": "1 Pedro",
    "2 Pe.": "2 Pedro",
    "1 Jn.": "1 Juan",
    "2 Jn.": "2 Juan",
    "3 Jn.": "3 Juan",
    "Jud.": "Judas",
    "Ap.": "Apocalipsis"
  };
  var er_grupolibro = "(G\xE9.|\xC9x.|Lev.|N\xFA.|Dt.|Jos.|Jue.|Rut|1 Sa.|2 Sa.|1 Re.|2 Re.|1 Cr.|2 Cr.|Esd.|Ne.|Est.|Job|Sal.|Pr.|Ec.|C.C.|Isa.|Jer.|La.|Eze.|Da.|Os.|Joel|Am.|Ab.|Jon.|Miq.|Na.|Hab.|Sof.|Hag.|Zac.|Mal.|Mt.|Mc.|Lc.|Jn.|Hec.|Ro.|1 Co.|2 Co.|G\xE1l.|Efe.|Flp.|Col.|1 Te.|2 Te.|1 Ti.|2 Ti.|Tit.|Flm.|Heb.|Snt.|1 Pe.|2 Pe.|1 Jn.|2 Jn.|3 Jn.|Jud.|Ap.)";
  var er_unlibropos = "([123]?[ ]?[A-Z\xC1\xC9\xCD\xD3\xDA\xD1\xDC][a-z\xE1\xE9\xED\xF3\xFA\xF1\xFC]*[.]?)";
  function textoIncluyeRefBiblica(texto, esref, libro, capitulo) {
    let libroscomp = [];
    for (const libro2 in Libro) {
      libroscomp.push(Libro[libro2]);
    }
    let vers\u00EDculos = "";
    let refb = "";
    let pref = "";
    let posf = "";
    let indice = texto.length;
    let resto = "";
    let librog = libro;
    let capitulog = capitulo;
    if (esref) {
      pref = "^";
      posf = "$";
    }
    const er_libro_capitulo_versiculos = new RegExp(
      pref + er_unlibropos + " ([0-9]*)[:]([0-9]*[-]?[0-9]*[:]?[0-9]*)" + posf
    );
    let corr = texto.match(er_libro_capitulo_versiculos);
    if (corr != null) {
      console.error("corr1");
      if (typeof Libro[corr[1].trim()] == "undefined" && !libroscomp.includes(corr[1].trim())) {
        resto = texto.slice(corr.index + corr[0].length);
        return [
          "",
          libro,
          capitulo,
          vers\u00EDculos,
          resto,
          `Problema: No se encontr\xF3 libro '${corr[1]}'`
        ];
      } else {
        librog = typeof Libro[corr[1].trim()] != "undefined" ? Libro[corr[1].trim()] : corr[1].trim();
        capitulog = corr[2];
        vers\u00EDculos = corr[3];
        refb = `${librog} ${capitulog}:${vers\u00EDculos}`;
        resto = texto.slice(corr.index + corr[0].length);
        indice = corr.index;
      }
    }
    const er_libro_capitulos = new RegExp(pref + er_grupolibro + " ([0-9]+-[0-9]+)" + posf);
    corr = texto.match(er_libro_capitulos);
    if (corr != null && corr.index < indice) {
      console.error("corr1.5", corr);
      librog = Libro[corr[1]];
      capitulog = corr[2];
      vers\u00EDculos = "";
      refb = `${librog} ${capitulog}`;
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_libro_capitulo_2_versiculos = new RegExp(pref + er_grupolibro + " ([0-9]*)[:]([0-9]*), ?([0-9]*)" + posf);
    corr = texto.match(er_libro_capitulo_2_versiculos);
    if (corr != null && corr.index < indice) {
      console.error("corr2");
      librog = Libro[corr[1]];
      capitulog = corr[2];
      vers\u00EDculos = `${corr[3]},${corr[4]}`;
      refb = `${librog} ${capitulog}:${vers\u00EDculos}`;
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_libro_capitulo_3_versiculos = new RegExp(pref + er_grupolibro + " ([0-9]*)[:]([0-9]*), ?([0-9]*), ?([0-9]*)" + posf);
    corr = texto.match(er_libro_capitulo_3_versiculos);
    if (corr != null && corr.index < indice) {
      console.error("corr3");
      librog = Libro[corr[1]];
      capitulog = corr[2];
      vers\u00EDculos = `${corr[3]},${corr[4]},${corr[5]}`;
      refb = `${librog} ${capitulog}:${vers\u00EDculos}`;
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_libro_capitulo_4_versiculos = new RegExp(pref + er_grupolibro + " ([0-9]*)[:]([0-9]*), ?([0-9]*), ?([0-9]*), ?([0-9]*)" + posf);
    corr = texto.match(er_libro_capitulo_4_versiculos);
    if (corr != null && corr.index < indice) {
      console.error("corr4");
      librog = Libro[corr[1]];
      capitulog = corr[2];
      vers\u00EDculos = `${corr[3]},${corr[4]},${corr[5]},${corr[6]}`;
      refb = `${librog} ${capitulog}:${vers\u00EDculos}`;
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_versiculos = new RegExp(
      pref + "(v[.] ?)([0-9]+[-][0-9]+)" + posf
    );
    corr = texto.match(er_versiculos);
    if (corr != null && corr.index < indice) {
      console.error("corr4.5");
      console.error("corr=", corr);
      if (typeof libro == "undefined" || libro == "") {
        return [
          "",
          libro,
          capitulo,
          texto,
          "",
          `No se encontr\xF3 una referencia anterior ${texto}`
        ];
      }
      refb = libro + " " + capitulo + ":" + corr[2];
      librog = libro;
      capitulog = capitulo;
      vers\u00EDculos = corr[2];
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_versiculo = new RegExp(pref + "(v[.] ?)([0-9]+)" + posf);
    corr = texto.match(er_versiculo);
    if (corr != null && corr.index < indice) {
      console.error("corr5");
      console.error("corr=", corr);
      if (typeof libro == "undefined" || libro == "") {
        return [
          "",
          libro,
          capitulo,
          texto,
          "",
          `No se encontr\xF3 una referencia anterior ${texto}`
        ];
      }
      refb = libro + " " + capitulo + ":" + corr[2];
      librog = libro;
      capitulog = capitulo;
      vers\u00EDculos = corr[2];
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_libro_capitulo = new RegExp(
      pref + er_unlibropos + " ([0-9]+)" + posf
    );
    corr = texto.match(er_libro_capitulo);
    if (corr != null && corr.index < indice) {
      console.error("corr6");
      if (typeof Libro[corr[1].trim()] == "undefined" && !libroscomp.includes(corr[1].trim())) {
        resto = texto.slice(corr.index + corr[0].length);
        return [
          "",
          libro,
          capitulo,
          vers\u00EDculos,
          resto,
          `Problema: No se encontr\xF3 libro '${corr[1]}'`
        ];
      }
      librog = typeof Libro[corr[1].trim()] != "undefined" ? Libro[corr[1].trim()] : corr[1].trim();
      refb = texto.replace(corr[1], librog);
      capitulog = corr[2];
      vers\u00EDculos = "";
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    const er_capitulo_versiculos = new RegExp(pref + "([0-9]+)[:]([0-9]+[-,]?[0-9]*)" + posf);
    corr = texto.match(er_capitulo_versiculos);
    if (corr != null && corr.index < indice) {
      console.error("corr7");
      if (typeof libro == "undefined" || libro == "") {
        return [
          "",
          libro,
          capitulo,
          "",
          texto,
          `No se encontr\xF3 una referencia anterior ${texto}`
        ];
      }
      refb = libro + " " + corr[1] + ":" + corr[2];
      librog = libro;
      capitulog = corr[1];
      vers\u00EDculos = corr[2];
      resto = texto.slice(corr.index + corr[0].length);
      indice = corr.index;
    }
    libro = librog;
    capitulo = capitulog;
    console.error("refb=", refb);
    console.error("libro=", libro);
    console.error("capitulo=", capitulo);
    console.error("versiculos=", vers\u00EDculos);
    console.error("resto=", resto);
    return [refb, libro, capitulo, vers\u00EDculos, resto, ""];
  }
  return __toCommonJS(comunes_exports);
})();
