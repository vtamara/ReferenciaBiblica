/*
 * App Script para facilitar consultar Referencias Bíblicas
 * 
 * Facilita traducir y escribir comentarios bíblicos al chequear sintaxis
 * de una referencia bíblíca seleccionada, extraerla de biblegateway.com y
 * agregarla a un semi-índice (como no es posible generar número de página
 * con la API de GoogleDocs no es posible generar un índice desde aquí).
 * 
 * Usa la librería cheerio (para reconocer el HTML retornado por biblegateway.com --aunque
 * es muy limitada pude entender lo suficiente para hacer rapidamente lo que se necesitaba).
 * Para poder usar este App Script con un documento agregarla
 *   https://github.com/tani/cheeriogs
 *   Script ID: 1ReeQ6WO8kKNxoaA_O0XEQ589cIrRvEBA9qcWpNqdOP17i47u6N9M5Xh0
 */

const LICENCIA = `
ISC License

Copyright (c) 2023, Vladimir Támara Patiño vtamara@pasosdeJesus.org

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR 
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF 
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
`

/* Inicializa */
function onOpen() {
  var ui = DocumentApp.getUi();
  ui.createMenu('Referencia Bíblica')
    .addItem('Estadísticas de Semi-Indice', 'estadisticasSemiIndice')
    .addItem('Presentar Referencia Bíblica', 'presentarReferenciaBíblica')
    .addToUi();
}

var urlIndice = 'https://docs.google.com/spreadsheets/d/1FpDEYD4FNcgKNw0NeOnlj0fh6KQjlm-Xm2A5LbtZq5E/edit#gid=1192987395';

function estadisticasSemiIndice() {
  
  var ss = SpreadsheetApp.openByUrl(urlIndice);
  var nombre = ss.getName();
  
  var sheet = ss.getSheets()[0];
  if (sheet.getRange("A1").getValue() != "File") {
    Logger.log("Se esperaba Page en A1");
  }
  if (sheet.getRange("B1").getValue() != "Start") {
    Logger.log("Se esperaba Row en B1");
  }
  if (sheet.getRange("C1").getValue() != "End") {
    Logger.log("Se esperaba Column en C1");
  }
  if (sheet.getRange("D1").getValue() != "Reference") {
    Logger.log("Se esperaba Reference en D1");
  }
  if (sheet.getRange("E1").getValue() != "Book") {
    Logger.log("Se esperaba Book en E1");
  }
  if (sheet.getRange("F1").getValue() != "Chapter") {
    Logger.log("Se esperaba Chapter en F1");
  }
  if (sheet.getRange("G1").getValue() != "Verse(s)") {
    Logger.log("Se esperaba Verse(s) en G1");
  }
  // This logs the value in the very last cell of this sheet
  var lastRow = sheet.getLastRow();
  var dat = [];
  for( i = 2; i <= lastRow; i++) {
    let p = sheet.getRange("A" + i).getValue();
    let r = sheet.getRange("B" + i).getValue();
    let c = sheet.getRange("C" + i).getValue();
    let ref = sheet.getRange("D" + i).getValue();
    let b = sheet.getRange("E" + i).getValue();
    let v = sheet.getRange("F" + i).getValue();
    if (typeof dat[p] == "undefined") {
      dat[p] = [];
    }
    if (typeof dat[p][r] == "undefined") {
      dat[p][r] = [];
    }
    dat[p][r][c] = {ref: ref, book: b, verses: v};
  }

  DocumentApp.getUi()
          .alert(`URL del semi-índice: ${urlIndice}\n` +
          `Nombre del archivo: ${nombre}\n` +
          `Última fila: ${lastRow}`);
}

function agregaReferenciaASemiIndice(inicio, fin, ref, libro, capitulo, versiculos) {
  /* Las offsets de los elementos no nos sirven porque pueden volver a 0 , no generan un orden */
  var ss = SpreadsheetApp.openByUrl(urlIndice);
  var archivo = DocumentApp.getActiveDocument().getName();
  var hoja = ss.getSheets()[0];
  var ultimaFila = hoja.getLastRow();
  hoja.appendRow([archivo, inicio, fin, ref, libro, capitulo, "'" + versiculos]);
  ultimaFila++;
  /* var rango = hoja.getRange("A2:F" + ultimaFila);
  rango.sort([{column: 1, ascending: true}, {column: 2, ascending: true}]) */
  
  let sp = PropertiesService.getScriptProperties();
  let k = sp.getKeys();
  sp.setProperty("últimoLibro", libro)
  sp.setProperty("últimoCapítulo", capitulo)
}

function presentarEnBarraLateral(ref, traducciontb, texto, url) {
  var html = HtmlService.createHtmlOutput(
    `<h1>${ref} [${traducciontb}]</h1>\n` +
    `<p>${texto}</p>` +
    `<span><a href="${url}" target="_blank">${url}</a></span>`
    )
    .setTitle('Referencia Bíblica');
  DocumentApp.getUi()
      .showSidebar(html);
}

/* Verifica la sintaxis de la referencia seleccionada, si lo es presenta el texto bíblico de RVR1960 y
 * agrega una entrada en el índice de escrituras 
 */
function presentarReferenciaBíblica() {
  let sp = PropertiesService.getScriptProperties();
  let libro =  sp.getProperty("últimoLibro");
  let capítulo = sp.getProperty("últimoCapítulo");
  let versículos = "";
  let [t, inicio, fin] = obtenerTextoSeleccionado();
  let refb = "";
  for (let i = 0; i < t.length; i++) {
    let resto = ""; // Resto debe ser blanco porque pasamos solo referencia bib.
    [refb, libro, capítulo, versiculos, resto, menserr] = 
      comunes.textoIncluyeRefBiblica(t[i], true, libro, capítulo);
    if (menserr !== "") {
      DocumentApp.getUi().alert(menserr);
      return "";
    }
    if (refb == "") {
      DocumentApp.getUi()
        .alert(`Referencia con formato desconocido: ${t[i]}`);
      return "";  
    }
    let traducciontb = "RVR1960"
    let c = extraerVersos(refb, traducciontb);
    /*DocumentApp.getUi()
      .alert(`Texto Bíblico: ${c.cita}`);*/
    presentarEnBarraLateral(refb,traducciontb, c.cita, c.url)
    if (c.cita != "") {
      /* Google Docs no permite manejar números de página desde App Script https://issuetracker.google.com/issues/36755897 */
      agregaReferenciaASemiIndice(inicio, fin, t[i], libro, capítulo, versículos);
    }
    return c;  
  }
}

/* Retorna arreglo de textos seleccionados en el Google Document contenedor.
 * Tomado de https://developers.google.com/apps-script/add-ons/editors/docs/quickstart/translate#translate.gs 
 */
function obtenerTextoSeleccionado() {
  const selection = DocumentApp.getActiveDocument().getSelection();
  const text = [];
  let lastStartIndex = 0;
  let lastEndIndex = 0;
  if (selection) {
    const elements = selection.getSelectedElements();
    for (let i = 0; i < elements.length; ++i) {
      if (elements[i].isPartial()) {
        const element = elements[i].getElement().asText();
        const startIndex = elements[i].getStartOffset();
        const endIndex = elements[i].getEndOffsetInclusive();
        lastStartIndex = startIndex;
        lastEndIndex = endIndex;

        text.push(element.getText().substring(startIndex, endIndex + 1));
      } else {
        const element = elements[i].getElement();
        // Only translate elements that can be edited as text; skip images and
        // other non-text elements.
        if (element.editAsText) {
          const elementText = element.asText().getText();
          // This check is necessary to exclude images, which return a blank
          // text element.
          if (elementText) {
            text.push(elementText);
          }
        }
      }
    }
  }
  if (!text.length) throw new Error('Please select some text.');
  return [text, lastStartIndex, lastEndIndex];
}

/* Extraer versículos de BibleGateway en version RVR1960.  
 * Comenzó a partir del ejemplo https://github.com/yassinedevop/auto-cite/blob/main/script.js 
 */
function extraerVersos(ref, traducciontb) {
  let ref2 = ref.replace(" ", "+").replace(":", "%3A");
  let url = `https://www.biblegateway.com/passage/?search=${ref2}&version=${traducciontb}`
  let respuesta = UrlFetchApp.fetch(url);
  let contenido = respuesta.getContentText();
  
  const c = Cheerio.load(contenido);
  const cita_t = c('.passage-text p'); // Analyzing response of biblegateway.com
  let cita = cita_t.text();
  return {url: url, cita: cita};
}
