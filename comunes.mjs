 /* Constantes y funciones comunes
  *
  * Mecanismos para usarlo en node.js y en ASG de
  * https://jeffy.info/2021/07/12/npm-from-apps-script.html
  *
  */

export const LICENCIA = `
ISC License

Copyright (c) 2023, Vladimir Támara Patiño vtamara@pasosdeJesus.org

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR 
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF 
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
`


/* Libros bíblicos y sus abreviaturas  */
export const Libro = {
  "Gé.": "Génesis",
  "Éx.": "Éxodo",
  "Lev.": "Levítico",
  "Nú.": "Números",
  "Dt.": "Deuteronomio",
  "Jos.": "Josué",
  "Jue.": "Jueces",
  "Rut": "Rut",
  "1 Sa.": "1 Samuel",
  "2 Sa.": "2 Samuel",
  "1 Re.": "1 Reyes",
  "2 Re.": "2 Reyes",
  "1 Cr.": "1 Crónicas",
  "2 Cr.": "2 Crónicas",
  "Esd.": "Esdras",
  "Ne.": "Nehemías",
  "Est.": "Esther",
  "Job": "Job",
  "Sal.": "Salmos",
  "Pr.": "Proverbios",
  "Ec.": "Eclesiastés",
  "C.C.": "Cantar de los Cantares",
  "Isa.": "Isaías",
  "Jer.": "Jeremías",
  "La.": "Lamentaciones",
  "Eze.": "Ezekiel",
  "Da.": "Daniel",
  "Os.": "Oseas",
  "Joel": "Joel",
  "Am.": "Amós",
  "Ab.": "Abdías",
  "Jon.": "Jonás",
  "Miq.": "Miqueas",
  "Na.": "Nahum",
  "Hab.": "Habacuc",
  "Sof.": "Sofonías",
  "Hag.": "Hageo",
  "Zac.": "Zacarías",
  "Mal.": "Malaquías",
  "Mt.": "Mateo",
  "Mc.": "Marcos",
  "Lc.": "Lucas",
  "Jn.": "Juan",
  "Hec.": "Hechos",
  "Ro.": "Romanos",
  "1 Co.": "1 Corintios",
  "2 Co.": "2 Corintios",
  "Gál.": "Gálatas",
  "Efe.": "Efesios",
  "Flp.": "Filipenses",
  "Col.": "Colosenses",
  "1 Te.": "1 Tesalonicenses",
  "2 Te.": "2 Tesalonicenses",
  "1 Ti.": "1 Timoteo",
  "2 Ti.": "2 Timoteo",
  "Tit.": "Tito",
  "Flm.": "Filemón",
  "Heb.": "Hebreos",
  "Snt.": "Santiago",
  "1 Pe.": "1 Pedro",
  "2 Pe.": "2 Pedro",
  "1 Jn.": "1 Juan",
  "2 Jn.": "2 Juan",
  "3 Jn.": "3 Juan",
  "Jud.": "Judas",
  "Ap.": "Apocalipsis"
};

export const er_grupolibro = "(Gé\.|Éx\.|Lev\.|Nú\.|Dt\.|Jos\.|Jue\.|Rut|1 Sa\.|2 Sa\.|1 Re\.|2 Re\.|1 Cr\.|2 Cr\.|Esd\.|Ne\.|Est\.|Job|Sal\.|Pr\.|Ec\.|C\.C\.|Isa\.|Jer\.|La\.|Eze\.|Da\.|Os\.|Joel|Am\.|Ab\.|Jon\.|Miq\.|Na\.|Hab\.|Sof\.|Hag\.|Zac\.|Mal\.|Mt\.|Mc\.|Lc\.|Jn\.|Hec\.|Ro\.|1 Co\.|2 Co\.|Gál\.|Efe\.|Flp\.|Col\.|1 Te\.|2 Te\.|1 Ti\.|2 Ti\.|Tit\.|Flm\.|Heb\.|Snt\.|1 Pe\.|2 Pe\.|1 Jn\.|2 Jn\.|3 Jn\.|Jud\.|Ap\.)";

export const er_unlibropos = "([123]?[ ]?[A-ZÁÉÍÓÚÑÜ][a-záéíóúñü]*[.]?)";

export function textoIncluyeRefBiblica(texto, esref, libro, capitulo) {
  let libroscomp = [];
  for (const libro in Libro ) {
    libroscomp.push(Libro[libro]);
  }
  let versículos = "";
  let refb = "";
  let pref = "";
  let posf = "";
  let indice = texto.length;
  let resto = "";
  let librog = libro;
  let capitulog = capitulo;

  if (esref) {  //La cadena completa debe ser una ref. bíblica
    pref = "^";
    posf = "$";
  }

  const er_libro_capitulo_4_versiculos = new RegExp(pref + er_grupolibro +
    " ([0-9]*)[:]([0-9]*), ?([0-9]*), ?([0-9]*), ?([0-9]*)" + posf);
  let corr = texto.match(er_libro_capitulo_4_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr4v");
    librog = Libro[corr[1]];
    capitulog = corr[2];
    versículos = `${corr[3]},${corr[4]},${corr[5]},${corr[6]}`
    refb = `${librog} ${capitulog}:${versículos}`;
    resto = texto.slice(corr.index+corr[0].length)
    indice= corr.index
  }

  const er_libro_capitulo_3_versiculos = new RegExp(pref + er_grupolibro +
    " ([0-9]*)[:]([0-9]*), ?([0-9]*), ?([0-9]*)" + posf);
  corr = texto.match(er_libro_capitulo_3_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr3v");
    librog = Libro[corr[1]];
    capitulog = corr[2];
    versículos = `${corr[3]},${corr[4]},${corr[5]}`
    refb = `${librog} ${capitulog}:${versículos}`;
    resto = texto.slice(corr.index+corr[0].length)
    indice= corr.index
  }

  const er_libro_capitulo_2_versiculos = new RegExp(pref + er_grupolibro +
    " ([0-9]*)[:]([0-9]*-?[0-9]*), ?([0-9]*)" + posf);
  corr = texto.match(er_libro_capitulo_2_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr2v");
    librog = Libro[corr[1]];
    capitulog = corr[2];
    versículos = `${corr[3]},${corr[4]}`
    refb = `${librog} ${capitulog}:${versículos}`;
    resto = texto.slice(corr.index+corr[0].length);
    indice = corr.index;
    console.error("indice=", indice);

  }

  const er_libro_capitulo_versiculos = new RegExp(pref + er_unlibropos +
    " ([0-9]*)[:]([0-9]*[-]?[0-9]*[:]?[0-9]*)" + posf
  )
  corr = texto.match(er_libro_capitulo_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr1");
    if (typeof Libro[corr[1].trim()] == "undefined" && 
      !libroscomp.includes(corr[1].trim())) {
      // Salimos para que caso 7 no caiga en error con Sura 2:223 
      resto = texto.slice(corr.index + corr[0].length)
      return ["", libro, capitulo, versículos, resto, 
        `Problema: No se encontró libro '${corr[1]}'`]
    } else {
      librog = typeof Libro[corr[1].trim()] != "undefined"  ?
        Libro[corr[1].trim()] : corr[1].trim();
      capitulog = corr[2];
      versículos = corr[3]
      refb = `${librog} ${capitulog}:${versículos}`;
      resto = texto.slice(corr.index+corr[0].length)
      indice= corr.index
    }
  }

  const er_libro_capitulos = new RegExp(pref + er_grupolibro +
    " ([0-9]+-[0-9]+)" + posf)
  corr = texto.match(er_libro_capitulos);
  if (corr != null && corr.index < indice) {
    console.error("corr1.5", corr);
    librog = Libro[corr[1]];
    capitulog = corr[2];
    versículos = "";
    refb = `${librog} ${capitulog}`;
    resto = texto.slice(corr.index+corr[0].length)
    indice = corr.index
  }

  const er_versiculos = new RegExp(pref + "(v[.] ?)([0-9]+[-][0-9]+)" + 
    posf 
  )
  corr = texto.match(er_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr4.5");
    console.error("corr=", corr);
    if ((typeof libro) == "undefined" || libro == "") {
      return ["", libro, capitulo, texto, "",
        `No se encontró una referencia anterior ${texto}`]
    }
    refb = libro + " " + capitulo + ":" + corr[2];
    librog = libro
    capitulog = capitulo
    versículos = corr[2];
    resto = texto.slice(corr.index+corr[0].length)
    indice = corr.index
  }

  const er_versiculo = new RegExp(pref + "(v[.] ?)([0-9]+)" + posf)
  corr = texto.match(er_versiculo);
  if (corr != null && corr.index < indice) {
    console.error("corr5");
    console.error("corr=", corr);
    if ((typeof libro) == "undefined" || libro == "") {
      return ["", libro, capitulo, texto, "",
        `No se encontró una referencia anterior ${texto}`]
    }
    refb = libro + " " + capitulo + ":" + corr[2];
    librog = libro
    capitulog = capitulo
    versículos = corr[2];
    resto = texto.slice(corr.index+corr[0].length)
    indice = corr.index
  }

  const er_libro_capitulo = new RegExp(pref + er_unlibropos +
    " ([0-9]+)" + posf
  );
  corr = texto.match(er_libro_capitulo);
  if (corr != null && corr.index < indice) {
    console.error("corr6");
    if (typeof Libro[corr[1].trim()] == "undefined" && 
      !libroscomp.includes(corr[1].trim())) {
      // Salimos para que caso 7 no caiga en error con Sura 2:223 
      resto = texto.slice(corr.index + corr[0].length)
      return ["", libro, capitulo, versículos, resto, 
        `Problema: No se encontró libro '${corr[1]}'`]
    }
    librog = typeof Libro[corr[1].trim()] != "undefined"  ?
      Libro[corr[1].trim()] : corr[1].trim();
    refb = texto.replace(corr[1], librog);
    capitulog = corr[2];
    versículos = ""
    resto = texto.slice(corr.index+corr[0].length)
    indice = corr.index

  }

  const er_capitulo_versiculos = new RegExp(pref + "([0-9]+)[:]([0-9]+[-,]?[0-9]*)" + posf);
  corr = texto.match(er_capitulo_versiculos);
  if (corr != null && corr.index < indice) {
    console.error("corr7");
    if ((typeof libro) == "undefined" || libro == "") {
      return ["", libro, capitulo, "", texto, 
        `No se encontró una referencia anterior ${texto}`]
    }
    refb = libro + " " + corr[1] + ":" + corr[2];
    librog = libro;
    capitulog = corr[1];
    versículos = corr[2];
    resto = texto.slice(corr.index+corr[0].length)
    indice = corr.index

  }
  libro = librog;
  capitulo = capitulog;
  console.error("refb=", refb);
  console.error("libro=", libro);
  console.error("capitulo=", capitulo);
  console.error("versiculos=", versículos);
  console.error("resto=", resto);


  return [refb, libro, capitulo, versículos, resto, ""]
}
