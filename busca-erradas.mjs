#!/usr/bin/env node
// vim: set expandtab tabstop=2 shiftwidth=2 foldmethod=marker fileencoding=utf-8:

// Examina texto con referencias biblicas y genera indice de estas.
// Dominio público de acuerdo a la legislación colombiana. 2023. vtamara@pasosdeJesus.org

import * as fs from 'fs';
import * as path from 'path';
import * as comunes from './comunes.mjs';

console.error(comunes.Libro)

if (process.argv.length != 3) {
  console.error("Primer parámetro debería ser ruta al texto")
  process.exit(1)
}
 // process.argv[0] es ruta a node
 // process.argv[1] es ruta completa a este guion
 // process.argv[2] es primer parámetro

var arc = path.resolve(process.argv[2])

let cont = fs.readFileSync(arc, "UTF-8");
let paginas = cont.split("\x0a\x0a\x0c")
console.error(paginas.length);

let libro = ""; // Último libro referenciado
let capitulo = ""; // Último capítulo referenciado
let refb = ""; // Última referencia bíblica
let indice = {}

for(let pagmu = 0; pagmu < paginas.length; pagmu++) {
  let rpag = paginas[pagmu];
  while (rpag != "") {
    const er = new RegExp("([1-3]  )?([A-Z][a-záéíóú]*\.?) [0-9]:[0-9]")
    let corr = rpag.match(er);
    if (corr != null) {
      if (typeof corr[1] == "undefined") {
        libro=corr[2];
      } else {
        libro=corr[1] + corr[2];
      }
      if (typeof comunes.Libro[libro] == "undefined") {
        console.log(`En página ${pagmu+1} libro desconocido '${libro}' en referencia '${rpag.slice((corr.index > 10 ? corr.index - 10 : corr.index), (rpag.length > corr.index+corr[0].length + 10 ? corr.index+corr[0].length + 10 : corr.index + corr[0].length) )}'`);
      }
      rpag = rpag.slice(corr.index + corr[0].length)
    } else {
      rpag = "";
    }
  }
}
