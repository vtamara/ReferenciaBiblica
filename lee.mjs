#! /usr/bin/env node
// vim: set expandtab tabstop=2 shiftwidth=2 foldmethod=marker fileencoding=utf-8:

// Examina texto con referencias biblicas y genera indice de estas.
// Dominio público de acuerdo a la legislación colombiana. 2023. vtamara@pasosdeJesus.org

import * as fs from 'fs';
import * as path from 'path';
import * as comunes from './comunes.mjs';

console.error(comunes.Libro)

if (process.argv.length != 3) {
  console.error("Primer parámetro debería ser ruta al texto")
  process.exit(1)
}
 // process.argv[0] es ruta a node
 // process.argv[1] es ruta completa a este guion
 // process.argv[2] es primer parámetro

var arc = path.resolve(process.argv[2])

let cont = fs.readFileSync(arc, "UTF-8");
let paginas = cont.split("\x0c")
console.error(paginas.length);

let libro = ""; // Último libro referenciado
let capitulo = ""; // Último capítulo referenciado
let refb = ""; // Última referencia bíblica
let indice = {}

for(let pagmu = 0; pagmu < paginas.length; pagmu++) {
  console.error("  ", pagmu);
  let rpag = paginas[pagmu];
  while (rpag != "") {
    //console.error("    ", rpag);
    let versiculos = ""; 
    let menserr = ""; // Mensaje de error
    [refb, libro, capitulo, versiculos, rpag, menserr] = 
      comunes.textoIncluyeRefBiblica(rpag, false, libro, capitulo)
    if (menserr != "") {
      console.error(`Problema en página ${pagmu+1}`);
      console.error("refb=", refb);
      console.error("libro=", libro);
      console.error("capitulo=", capitulo);
      console.error("versiculos=", versiculos);
      console.error("rpag=", rpag);
      console.error("mesnerr=", menserr);
      //rpag = "";
    }
    if (refb != "") {
      if (typeof indice[libro] == "undefined") {
        indice[libro] = {}
      }
      if (typeof indice[libro][capitulo] == "undefined") {
        indice[libro][capitulo] = {}
      }
      if (typeof indice[libro][capitulo][versiculos] == "undefined") {
        indice[libro][capitulo][versiculos] = []
      }
      indice[libro][capitulo][versiculos].push(pagmu + 1);
    }
  }
}

console.error(indice)
console.log("Libro,", "Página");
for (const libroav in comunes.Libro) {
  if (typeof indice[comunes.Libro[libroav]] != "undefined") {
    libro = comunes.Libro[libroav]
    console.log(libro + ",", "")
    let capord = [];
    for(const capitulo in indice[libro]) {
      let capceros = capitulo;
      let c = capitulo.match(new RegExp("[0-9]+"))
      if (c != null) {
        capceros = c[0].padStart(3, "0") + capitulo.slice(c.index+c[0].length)
      }
      capord.push(capceros);
    }
    capord = capord.sort();
    console.error("capord=", capord);
    capord.forEach(capceros => {
      let capitulo = capceros.replace(/^0+/, '');
      console.error(capitulo);
      let verord = []
      for (const versiculos in indice[libro][capitulo]) {
        let verceros = versiculos;
        let c = versiculos.match(new RegExp("[0-9]+"))
        if (c != null) {
          verceros = c[0].padStart(3, "0") + 
            versiculos.slice(c.index+c[0].length)
        }
        verord.push(verceros);
      }
      verord = verord.sort();
      verord.forEach(verceros => {
        let versiculos = verceros.replace(/^0+/, '');
        let pags = indice[libro][capitulo][versiculos]
        if (versiculos == "") {
          console.log(capitulo + ",", "\"" + pags.join(", ") + "\"");
        } else {
          console.log("\"" + capitulo + ":" + versiculos + "\",", 
            "\"" + pags.join(", ") + "\"");
        }
      });
    });
  }
}
